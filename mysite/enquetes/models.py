from django.db import models

class Pergunta(models.Model):
    texto = models.CharField(max_length = 150)
    data_pub = models.DateTimeField('Data de Publicação')
    def __str__(self):
        return '({}) - {}'.format(self.id, self.texto)


class Alternativa(models.Model):
    texto = models.CharField(max_length = 80)
    quant_votos = models.IntegerField('Quantidade de votos', default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return '({}) - {}'.format(self.id, self.texto)
