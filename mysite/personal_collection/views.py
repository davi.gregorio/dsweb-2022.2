# from django.http import HttpResponseRedirect
# from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from django.views import View
# from .models import Item, User, Item, PersonalContact, Loan
from .models import Item

class IndexView(View):
    def get(self, request, *args, **kwargs):
        items = Item.objects.all()
        context = {'items': items}
        return render(request, 'personal_collection/index.html', context)

class ItemView(View):
    def get(self, request, *args, **kwargs):
        item = get_object_or_404(Item, pk=kwargs['pk'])
        context = {'item': item}
        return render(request, 'personal_collection/item.html', context)