# from django.utils import timezone
from datetime import date
from django.db import models

class User(models.Model):
    name = models.CharField(max_length = 150)
    last_name = models.CharField(max_length = 150)
    email = models.EmailField(max_length = 256)
    password = models.CharField(max_length=50)
    def __str__(self):
        return '({}) - {}'.format(self.id, self.name)


class Item(models.Model):
    class ItemKind(models.TextChoices):
        BOOK = 'BK', ('Book')
        PERSONAL_ITEM = 'PI', ('Personal Item')

    cover_photo = models.ImageField(upload_to = 'personal_collection/assets',blank=True)
    name = models.CharField(max_length = 150)
    author_name = models.CharField(max_length = 250, blank=True)
    year = models.IntegerField(default = date.today().year)
    item_kind = models.CharField(
        max_length=2,
        choices=ItemKind.choices,
        default=ItemKind.PERSONAL_ITEM,
    )
    # Relationships
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return '({}) - {}'.format(self.id, self.name)


class PersonalContact(models.Model):
    name = models.CharField(max_length = 150)
    email = models.EmailField(max_length = 256)
    phone = models.CharField(max_length = 150)
    # Relationships
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return '({}) - {}'.format(self.id, self.name)

class Loan(models.Model):
    borrowed_at = models.DateField(auto_now_add = True)
    refudend_at = models.DateTimeField(null=True, blank=True)
    is_refudend = models.BooleanField(default = False)

    # Relationships
    personal_contact = models.ForeignKey(PersonalContact, on_delete=models.SET_NULL, null=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

