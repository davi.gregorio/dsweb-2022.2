from django.urls import path
from .views import IndexView, ItemView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('item/<int:pk>/',ItemView.as_view(), name='item'),
]