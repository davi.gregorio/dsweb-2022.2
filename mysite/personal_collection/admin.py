from django.contrib import admin
from .models import User
from .models import Item
from .models import PersonalContact
from .models import Loan

admin.site.register(User)
admin.site.register(Item)
admin.site.register(PersonalContact)
admin.site.register(Loan)